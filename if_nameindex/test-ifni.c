#include <errno.h>
#include <net/if.h>
#include <stdio.h>

int main(void)
{
	struct if_nameindex *ifs = 0;
	errno = 0;

	perror("just to be sure");
	ifs = if_nameindex();
	perror("if_nameindex");
	printf("ifs = %p\n", ifs);
	if(ifs != NULL)
	{
		unsigned c = 0;
		for(struct if_nameindex ifni = ifs[0]; ifni.if_name != NULL; ifni = ifs[++c])
		{
			printf("  %d = %s\n", c, ifni.if_name);
		}
	}
	if_freenameindex(ifs);
	return 0;
}
